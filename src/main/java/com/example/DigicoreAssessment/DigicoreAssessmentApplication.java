package com.example.DigicoreAssessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigicoreAssessmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(DigicoreAssessmentApplication.class, args);
	}

}
